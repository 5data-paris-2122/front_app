import { IpynbRenderer } from "react-ipynb-renderer";
import { Row, Col } from "react-bootstrap";

// Formula renderer for katex
import "katex/dist/katex.min.css";

// Jupyter theme
import "react-ipynb-renderer/dist/styles/monokai.css";
// import ipynb file as json
import ipynb from "./5data.json";

export default function Ipynb() {
  return (
    <Row>
      <Col lg={12}>
        <IpynbRenderer
          ipynb={ipynb}
          syntaxTheme="xonokai"
          language="python"
          bgTransparent={true}
          formulaOptions={{
            // optional
            renderer: "mathjax", // katex by default
            katex: {
              delimiters: "gitlab", // dollars by default
              katexOptions: {
                fleqn: false,
              },
            },
          }}
        />
      </Col>
    </Row>
  );
}
