import React from "react";
import { Row, Col } from "react-bootstrap";
import { FileEarmarkPlusFill, FileEarmarkMinusFill } from "react-bootstrap-icons";
import FormControl from "./formControl";

export default function BlocAddModules({ supcursNumber, name, target, handleModifyState, count, params, disabled, student }) {
  function getPlaceHolder(basicPlaceHolder, id, i) {
    if (student) {
      if (id.includes("grade")) {
        return student.sup_cursus[supcursNumber].modules[i].grades;
      } else {
        return student.sup_cursus[supcursNumber].modules[i].name;
      }
    } else {
      return basicPlaceHolder;
    }
  }

  return (
    <div>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <h6>{name} :</h6>
        {disabled ? null : (
          <div style={{ display: "flex", justifyContent: "space-between", margin: "4px 0px" }}>
            <button
              style={{ border: "0px", backgroundColor: "transparent" }}
              type="button"
              onClick={() => handleModifyState(target + ":+", supcursNumber)}
            >
              <FileEarmarkPlusFill size={22} color="gray" />
            </button>
            <button
              style={{ border: "0px", backgroundColor: "transparent" }}
              type="button"
              onClick={() => handleModifyState(target + ":-", supcursNumber)}
            >
              <FileEarmarkMinusFill size={22} color="gray" />
            </button>
          </div>
        )}
      </div>
      <div style={{ margin: "0 0 4px 12px", backgroundColor: "rgb(62 62 198 / 12%)", padding: "8px", borderRadius: "12px" }}>
        {[...Array(count).keys()].map((_, i) => (
          <Row key={i}>
            {params.map(({ label, type, placeholder, id }, j) => (
              <Col lg={6} key={j}>
                <FormControl
                  label={label + " " + (i + 1)}
                  type={type}
                  placeholder={getPlaceHolder(placeholder, id, i)}
                  id={id + supcursNumber + "_" + i}
                  disabled={disabled}
                />
              </Col>
            ))}
            {count > 1 && i < count - 1 ? <hr style={{ maxWidth: "150px", margin: "0px auto" }} /> : null}
          </Row>
        ))}
      </div>
    </div>
  );
}
