import { useState, useEffect } from "react";
import { Row } from "react-bootstrap";
import { PlotBar } from "./../../../middleware/components";

export default function Graphs({ typeGraph, attributes, students }) {
  const [Data, setData] = useState([]);
  const getMeanGrades = (student) =>
    student.sup_cursus.reduce((acc, curr) => {
      return (
        acc +
        parseFloat(
          curr.modules.reduce((accM, currM) => {
            return parseFloat(accM) + parseFloat(currM.grades);
          }, 0) / curr.modules.length
        )
      );
    }, 0) / student.sup_cursus.length;

  const getAttendances = (student) =>
    student.sup_cursus.reduce((acc, curr) => {
      return acc + curr.attendance;
    }, 0) / student.sup_cursus.length;

  const getJpoPresence = (student) =>
    student.sup_cursus.reduce((acc, curr) => {
      return curr.attended_open_day ? 1 : 0;
    }, 0) / student.sup_cursus.length;

  const getProContract = (student) =>
    student.sup_cursus.reduce((acc, curr) => {
      return curr.contract.job_title ? 1 : 0;
    }, 0) / student.sup_cursus.length;

  const getResultFor2Attributes = (student) => {
    let Attrs = [];
    for (let i = 0; i < attributes.length; i++) {
      const Attr = attributes[i];
      if (Attr === "Notes") {
        Attrs[i] = parseFloat(getMeanGrades(student).toFixed(2));
      }
      if (Attr === "Absences") {
        Attrs[i] = parseFloat(getAttendances(student).toFixed(2));
      }
      if (Attr === "JPO") {
        Attrs[i] = parseFloat(getJpoPresence(student).toFixed(2));
      }
      if (Attr === "Contrat Pro") {
        Attrs[i] = parseFloat(getProContract(student).toFixed(2));
      }
      if (Attr === "Region") {
        Attrs[i] = student.address.city;
      }
      if (Attr === "Changement d'école") {
        if (student.post_cursus.new_school !== "") {
          Attrs[i] = student.post_cursus.new_school;
        }
        Attrs[i] = Attrs[i] ? Attrs[i] : "Pas de changement d'école";
      }
      if (Attr === "Sortie de Supinfo") {
        if (student.post_cursus.quit_reason !== "") {
          Attrs[i] = student.post_cursus.quit_reason;
        }
        Attrs[i] = Attrs[i] ? Attrs[i] : "Pas de sortie de Supinfo";
      }
      if (Attr === "Lycée") {
        if (student.education.length > 0) {
          Attrs[i] = student.education.find(
            (edu) =>
              edu.schoolname === "Lycée Jean Jaurès" || edu.schoolname === "Lycée Emile Zola" || edu.schoolname === "Lycée Victor et Hélène Basch"
          );
        }
        Attrs[i] = Attrs[i] ? Attrs[i].schoolname : "Pas de lycée connu";
      }
      if (Attr === "Classes") {
        Attrs[i] = student.sup_cursus[0].class_name;
      }
    }
    return {
      Attr1: Attrs[0],
      Attr2: Attrs[1],
    };
  };

  const getResultFor1Attribute = (student) => {
    const Attr = attributes[0];
    let temp_data;
    if (Attr === "Region") {
      return { Attr1: student.address.city };
    }
    if (Attr === "Classes") {
      return { Attr1: student.sup_cursus[0].class_name };
    }
    if (Attr === "Changement d'école") {
      if (student.post_cursus.new_school !== "") {
        temp_data = student.post_cursus.new_school;
      }
      return { Attr1: temp_data ? temp_data : "Pas de changement d'école" };
    }
    if (Attr === "Sortie de Supinfo") {
      if (student.post_cursus.quit_reason !== "") {
        temp_data = student.post_cursus.quit_reason;
      }
      return { Attr1: temp_data ? temp_data : "Pas de sortie de Supinfo" };
    }
    if (Attr === "Lycée") {
      if (student.education.length > 0) {
        temp_data = student.education.find(
          (edu) =>
            edu.schoolname === "Lycée Jean Jaurès" || edu.schoolname === "Lycée Emile Zola" || edu.schoolname === "Lycée Victor et Hélène Basch"
        );
      }
      return { Attr1: temp_data ? temp_data.schoolname : "Pas de lycée connu" };
    }
  };

  const getAttributes = (student) => {
    if (typeGraph === 1) {
      return getResultFor1Attribute(student);
    }
    if (typeGraph === 2) {
      return getResultFor2Attributes(student);
    }
  };

  const getFormatedObject = (parsedStudents) => {
    let tempData = {};
    for (let i = 0; i < parsedStudents.length; i++) {
      const element = parsedStudents[i];
      if (!tempData[element.Attr1]) tempData[element.Attr1] = [];
      tempData[element.Attr1].push(element.Attr2);
    }
    return tempData;
  };

  const getFormatedList = (parsedStudents) => {
    let tempData = getFormatedObject(parsedStudents);
    let newData = [];
    for (let i = 0; i < Object.keys(tempData).length; i++) {
      const element = Object.keys(tempData)[i];
      newData.push({
        Attr1: element,
        Attr2: tempData[element].reduce((acc, curr) => acc + curr) / tempData[element].length,
      });
    }
    return newData;
  };

  const getFormatedObjectOne = (parsedStudents) => {
    let tempData = {};
    for (let i = 0; i < parsedStudents.length; i++) {
      const element = parsedStudents[i];
      if (!tempData[element.Attr1]) tempData[element.Attr1] = 0;
      tempData[element.Attr1] += 1;
    }
    return tempData;
  };

  const getFormatedListOne = (parsedStudents) => {
    let tempData = getFormatedObjectOne(parsedStudents);
    let newData = [];
    for (let i = 0; i < Object.keys(tempData).length; i++) {
      const element = Object.keys(tempData)[i];
      newData.push({
        Attr1: element,
        Attr2: tempData[element] / students.length,
      });
    }
    return newData;
  };

  function effect() {
    let parsedStudents = students.map((student) => getAttributes(student));
    let formatedStudents = [];
    if (typeGraph === 1) formatedStudents = getFormatedListOne(parsedStudents);
    if (typeGraph === 2) formatedStudents = getFormatedList(parsedStudents);
    setData(formatedStudents);
  }

  function cleanup() {}

  useEffect(() => {
    effect();
    return () => {
      cleanup();
    };
  }, [attributes, students]);

  return (
    <Row>
      <PlotBar data={Data} typeGraph={typeGraph} />
    </Row>
  );
}
