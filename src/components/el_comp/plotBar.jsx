import { useEffect } from "react";
import * as d3 from "d3";

export default function PlotBar({ data, typeGraph }) {
  const getColor = () => {
    return "#4040B8";
  };

  function getSize(isWidth) {
    let width = document.getElementById("plotBarTarget").getBoundingClientRect().width;
    if (isWidth) {
      if (width > 800) {
        width = 800;
      }
      return width;
    }
    return width > 800 ? 800 / 1.77777777778 : width / 1.77777777778;
  }

  function cleanup() {
    d3.select("#mainSVGBar").remove();
  }

  useEffect(() => {
    data.sort((a, b) => b.Attr2 - a.Attr2);
    let maxDomain = 1;
    if (typeGraph === 2) {
      maxDomain = Math.round(
        1.5 *
          data.reduce((acc, curr) => {
            if (curr.Attr2 > acc) return curr.Attr2;
            return acc;
          }, 0)
      );
    }
    const margin = { top: 10, right: 30, bottom: 90, left: 30 };
    const width = getSize(true) - margin.left - margin.right;
    const height = getSize() - margin.top - margin.bottom;

    const svg = d3
      .select("#plotBarTarget")
      .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .attr("id", "mainSVGBar")
      .append("g")
      .attr("id", "plotBarSchema")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // X axis
    var x = d3
      .scaleBand()
      .range([0, width])
      .domain(
        data.map(function (d) {
          return d.Attr1;
        })
      )
      .padding(0.2);
    svg
      .append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x))
      .selectAll("text")
      .attr("transform", "translate(-10,0)rotate(-45)")
      .style("text-anchor", "end");

    // Add Y axis
    var y = d3.scaleLinear().domain([0, maxDomain]).range([height, 0]);
    svg.append("g").call(d3.axisLeft(y));

    // Bars
    svg
      .selectAll("mybar")
      .data(data)
      .enter()
      .append("rect")
      .attr("x", function (d) {
        return x(d.Attr1);
      })
      .attr("width", x.bandwidth())
      .attr("fill", (d) => getColor(d))
      // no bar at the beginning thus:
      .attr("height", function (d) {
        return height - y(0);
      }) // always equal to 0
      .attr("y", function (_) {
        return y(0);
      });

    // Animation
    svg
      .selectAll("rect")
      .transition()
      .duration(800)
      .attr("y", function (d) {
        return y(d.Attr2);
      })
      .attr("height", function (d) {
        return height - y(d.Attr2);
      })
      .delay(function (_, i) {
        return i * 100;
      });

    return () => {
      cleanup();
    };
  }, [data, typeGraph]);

  return <div id="plotBarTarget" style={{ textAlign: "center", marginTop: "16px" }}></div>;
}
