import React from "react";
import { Route, Switch } from "react-router-dom";

export default function CustomRouter({ routes }) {
  return (
    <Switch>
      {Object.keys(routes).map((page, i) => (
        <Route key={i} exact={routes[page].is_exact} path={routes[page].path}>
          {routes[page].comp}
        </Route>
      ))}
    </Switch>
  );
}
