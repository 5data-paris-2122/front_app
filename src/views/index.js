import React, { useEffect, useState } from "react";
import { BrowserRouter } from "react-router-dom";

import { CustomRouterLayout, read } from "./../middleware/common";
import { Loading as LoadingComponent } from "./../middleware/components";

import LandingPage from "./landingpage";
import IndexStudents from "./students";
import IndexIpynb from "./ipynb";

export default function Index() {
  const [Loading, setLoading] = useState(true);
  const [Students, setStudents] = useState([]);

  async function fetchData() {
    setLoading(true);
    const readResult = await read("students");
    if (readResult.status === 200) {
      setStudents(readResult.message);
    }
    setLoading(false);
  }
  useEffect(() => {
    fetchData();
  }, []);

  if (Loading) return <LoadingComponent />;

  return (
    <BrowserRouter>
      <CustomRouterLayout
        routes={[
          { is_exact: true, path: "/", comp: <LandingPage students={Students} /> },
          { is_exact: false, path: "/students", comp: <IndexStudents students={Students} fetchData={fetchData} /> },
          { is_exact: false, path: "/ipynb", comp: <IndexIpynb /> },
        ]}
      />
    </BrowserRouter>
  );
}
