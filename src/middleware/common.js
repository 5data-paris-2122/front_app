/*------ CUSTOM ROUTERS ------*/
import CustomRouter from "./other/routers/customRouter";
import CustomRouterLayout from "./other/routers/customRouterLayout";

export { CustomRouter, CustomRouterLayout };

/*------ MENU ------*/
export { menu } from "./other/menu";

/*------ REQUESTS ------*/
export { createOne } from "./other/requests";
export { read } from "./other/requests";
export { updateOne } from "./other/requests";
export { deleteOne } from "./other/requests";

/*------ LOCAL STORAGE ------*/
export { setInLocalStorage } from "./other/requests";
export { getInLocalStorage } from "./other/requests";
export { delInLocalStorage } from "./other/requests";
export { clearLocalStorage } from "./other/requests";
