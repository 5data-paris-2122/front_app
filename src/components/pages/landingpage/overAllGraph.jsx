import { useState, useEffect } from "react";
import { Row, Col } from "react-bootstrap";
import { PlotRadar } from "./../../../middleware/components";

export default function OverAllGraph({ students }) {
  const [Data, setData] = useState([]);
  const [DataShown, setDataShown] = useState([]);
  const Axes = ["Paris", "Rennes", "Caen", "Lyon", "Marseille", "JPO", "Contrat Pro", "Changement d'école", "Sortie de Supinfo"];

  const formatedData = () => Axes.map((axe) => new Object({ axis: axe, value: 0 }));

  function handleclick(index) {
    let newDataShown = Object.assign([], DataShown);
    let condition = newDataShown[index].reduce((acc, curr) => (curr.value > 0 ? curr : acc), 0).value > 0;
    newDataShown[index] = condition ? formatedData() : Data[index];
    setDataShown(newDataShown);
  }
  function getValueOf(Axe, classname) {
    switch (Axe) {
      case "Paris":
        return classname.filter((student) => student.sup_cursus[0].campus === Axe).length / classname.length;
      case "Rennes":
        return classname.filter((student) => student.sup_cursus[0].campus === Axe).length / classname.length;
      case "Caen":
        return classname.filter((student) => student.sup_cursus[0].campus === Axe).length / classname.length;
      case "Lyon":
        return classname.filter((student) => student.sup_cursus[0].campus === Axe).length / classname.length;
      case "Marseille":
        return classname.filter((student) => student.sup_cursus[0].campus === Axe).length / classname.length;
      case "JPO":
        return classname.filter((student) => student.sup_cursus[0].attended_open_day).length / classname.length;
      case "Contrat Pro":
        return classname.filter((student) => student.sup_cursus[0].contract.job_title !== "").length / classname.length;
      case "Changement d'école":
        return classname.filter((student) => student.post_cursus.new_school !== "").length / classname.length;
      case "Sortie de Supinfo":
        return classname.filter((student) => student.post_cursus.quit_reason !== "").length / classname.length;
      default:
        break;
    }
  }

  function effect() {
    let classes = ["BoE1", "BoE2", "BoE3", "MoE1", "MoE2"];
    let listOfClasses = [];
    let finalData = [];
    for (let i = 0; i < classes.length; i++) {
      const classname = classes[i];
      listOfClasses.push(students.filter((student) => student.sup_cursus[0].class_name === classname));
    }
    for (let i = 0; i < listOfClasses.length; i++) {
      const classname = listOfClasses[i];
      finalData[i] = [];
      for (let j = 0; j < Axes.length; j++) {
        const axis = Axes[j];
        finalData[i].push({
          axis: axis,
          value: getValueOf(axis, classname),
        });
      }
    }
    setData(finalData);
    setDataShown(finalData);
  }

  function cleanup() {}

  useEffect(() => {
    effect();
    return () => {
      cleanup();
    };
  }, [students]);

  return (
    <Row>
      <Col lg={12}>
        <PlotRadar data={DataShown} handleclick={handleclick} />
      </Col>
    </Row>
  );
}
