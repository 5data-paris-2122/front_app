/*------ ELEMENT COMPONENTS FROM 'el_comp' FOLDER ------*/
import BlocAddStudent from "./../components/el_comp/blocAddStudent";
import BlocAddStudentCursus from "./../components/el_comp/blocAddStudentCursus";
import FormCheck from "./../components/el_comp/formCheck";
import FormControl from "./../components/el_comp/formControl";
import FormSelect from "./../components/el_comp/formSelect";
import PlotBar from "./../components/el_comp/plotBar";
import PlotRadar from "./../components/el_comp/plotRadar";
import Loading from "./../components/el_comp/loading";
import NavlinkOffcanvas from "./../components/el_comp/navlink_offcanvas";
import TableComp from "./../components/el_comp/table_comp";

/*------ LAYOUT COMPONENTS FROM 'layout' FOLDER ------*/
import Layout from "./../components/layout/index";
import MainNavigation from "./../components/layout/mainNavigation";

export { BlocAddStudent, BlocAddStudentCursus, FormCheck, FormControl, FormSelect, PlotBar, PlotRadar, Loading, NavlinkOffcanvas, TableComp };
export { Layout, MainNavigation };
