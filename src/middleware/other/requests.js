import axios from "axios";
axios.defaults.baseURL = process.env.React_App_Main_Api_Url;
//Create
export const createOne = async (url, data) => {
  let feedback = {};
  await axios
    .post(url, data)
    .then((response) => {
      feedback = {
        status: response.status,
        message: response.data,
      };
    })
    .catch((err) => {
      if (err.response) {
        feedback = {
          status: err.code,
          message: err.response.data,
        };
      }
    });
  return feedback;
};

//Read
export const read = async (url) => {
  let feedback = {};
  await axios
    .get(url)
    .then((response) => {
      feedback = {
        status: response.status,
        message: response.data,
      };
    })
    .catch((err) => {
      if (err.response) {
        feedback = {
          status: err.code,
          message: err.response.data,
        };
      }
    });
  return feedback;
};

//Update
export const updateOne = async (url, data) => {
  let feedback = {};
  await axios
    .put(url, data)
    .then((response) => {
      feedback = {
        status: response.status,
        message: response.data,
      };
    })
    .catch((err) => {
      if (err.response) {
        feedback = {
          status: err.code,
          message: err.response.data,
        };
      }
    });
  return feedback;
};

//Delete
export const deleteOne = async (url) => {
  let feedback = {};
  await axios
    .delete(url)
    .then((response) => {
      feedback = {
        status: response.status,
        message: response.data,
      };
    })
    .catch((err) => {
      if (err.response) {
        feedback = {
          status: err.code,
          message: err.response.data,
        };
      }
    });
  return feedback;
};

// LOCAL STORAGE //

export const setInLocalStorage = async (entity, data) => {
  localStorage.setItem(entity, data);
};

export const getInLocalStorage = async (entity) => {
  const item = localStorage.getItem(entity);
  return item;
};

export const delInLocalStorage = async (entity) => {
  localStorage.removeItem(entity);
};

export const clearLocalStorage = async () => {
  await localStorage.clear();
  return true;
};
