import React from "react";
import { Row, Col } from "react-bootstrap";

import { MainNavigation } from "./../../middleware/components";

export default function Layout({ children }) {
  return (
    <>
      <Row className="px-0 mx-0">
        <MainNavigation />
      </Row>
      <Row className="justify-content-center mt-4 px-0 mx-0">
        <Col lg={10}> {children} </Col>
      </Row>
    </>
  );
}
