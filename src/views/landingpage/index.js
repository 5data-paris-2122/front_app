import LandingPage from "./../../components/pages/landingpage";

export default function IndexLanding({ students }) {
  return <LandingPage students={students} />;
}
