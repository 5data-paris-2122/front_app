import { useState, useEffect } from "react";

import CreateOne from "./../../components/pages/students/createOne";
import ReadOne from "./../../components/pages/students/readOne";
import ViewAll from "./../../components/pages/students/viewAll";

import { read, CustomRouter, createOne as createOneRequest, updateOne, deleteOne } from "./../../middleware/common";
import { Loading as LoadingComp } from "./../../middleware/components";

export default function IndexStudents({ students, fetchData }) {
  const [Students, setStudents] = useState(students);
  const [Loading, setLoading] = useState(false);
  const [FeedBackMSG, setFeedBackMSG] = useState();

  const fetchStudents = async () => {
    fetchData();
  };

  const createStudent = async (student) => {
    const createOneResult = await createOneRequest("/students", student);
    if (createOneResult.status === 201) {
      fetchStudents();
      setFeedBackMSG(createOneResult.message);
    } else {
      console.error(createOneResult.message);
      setFeedBackMSG(createOneResult.message);
    }
  };

  const updateStudent = async (student) => {
    const updateOneResult = await updateOne("/students/" + student._id, student);
    if (updateOneResult.status === 201) {
      fetchStudents();
      setFeedBackMSG(updateOneResult.message);
    } else {
      console.error(updateOneResult.message);
      setFeedBackMSG(updateOneResult.message);
    }
  };

  const deleteStudent = async (id) => {
    const deleteOneResult = await deleteOne("/students/" + id);
    if (deleteOneResult.status === 200) {
      fetchStudents();
      setFeedBackMSG(deleteOneResult.message);
    } else {
      console.error(deleteOneResult.message);
      setFeedBackMSG(deleteOneResult.message);
    }
  };

  const parseStudent = (currentStudent) => {
    let student = {
      supId: currentStudent.supId,
      name: currentStudent.name,
      lastname: currentStudent.lastname,
      birthdate: currentStudent.birthdate,
      address: {
        street: currentStudent.address.street,
        city: currentStudent.address.city,
        country: currentStudent.address.country,
      },
      education: currentStudent.education,
      internships: currentStudent.internships,
      sup_cursus: [],
      post_cursus: {
        future_job: {
          job_title: currentStudent.post_cursus.future_job.job_title ? currentStudent.post_cursus.future_job.job_title : "",
          company_name: currentStudent.post_cursus.future_job.company_name ? currentStudent.post_cursus.future_job.company_name : "",
        },
        quit_reason: currentStudent.post_cursus.quit_reason,
        new_school: currentStudent.post_cursus.new_school,
      },
    };
    for (let j = 0; j < currentStudent.sup_cursus.length; j++) {
      const supcurs = currentStudent.sup_cursus[j];
      student.sup_cursus.push({
        class_name: supcurs.class_name,
        campus: supcurs.campus,
        attendance: supcurs.attendance,
        attended_open_day: supcurs.attended_open_day,
        contract: Object.keys(supcurs.contract).length > 0 ? supcurs.contract : { job_title: "", company_name: "" },
        modules: supcurs.modules,
      });
    }
    return student;
  };

  const generateStudents = async () => {
    setLoading(true);
    for (let i = 0; i < 100; i++) {
      const generateResult = await read("https://student.5data.northamp.fr/generate_student");
      if (generateResult.status === 200) {
        createOneRequest("/students", parseStudent(generateResult.message));
      } else {
        console.error(generateResult.message);
        setFeedBackMSG(generateResult.message);
      }
    }
    fetchStudents();
    setLoading(false);
  };

  const dropTable = () => {
    setLoading(true);
    for (let i = 0; i < Students.length; i++) {
      deleteOne("/students/" + Students[i]._id);
    }
    setTimeout(() => {
      fetchStudents();
    }, Students.length * 25);
    setLoading(false);
  };

  useEffect(() => {
    setStudents(students);
  }, [students]);

  if (Loading) return <LoadingComp />;

  return (
    <CustomRouter
      routes={[
        {
          is_exact: true,
          path: "/students",
          comp: (
            <ViewAll
              data={Students}
              deleteStudent={deleteStudent}
              FeedBackMSG={FeedBackMSG}
              setFeedBackMSG={setFeedBackMSG}
              generateStudents={generateStudents}
              dropTable={dropTable}
            />
          ),
        },
        {
          is_exact: true,
          path: "/students/create",
          comp: <CreateOne createStudent={createStudent} FeedBackMSG={FeedBackMSG} setFeedBackMSG={setFeedBackMSG} />,
        },
        {
          is_exact: true,
          path: "/students/:id",
          comp: <ReadOne updateStudent={updateStudent} FeedBackMSG={FeedBackMSG} setFeedBackMSG={setFeedBackMSG} />,
        },
      ]}
    />
  );
}
