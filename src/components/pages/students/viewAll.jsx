import { useEffect } from "react";
import { Card, Row, Col, Button } from "react-bootstrap";
import { PersonPlusFill, JournalX } from "react-bootstrap-icons";
import { Link } from "react-router-dom";

import { TableComp } from "./../../../middleware/components";

export default function ViewAll({ data, deleteStudent, FeedBackMSG, setFeedBackMSG, generateStudents, dropTable }) {
  const deleteOne = async (element) => {
    if (window.confirm(`Supprimer le document ${element.supId} ?`)) {
      await deleteStudent(element._id);
    }
  };

  const deleteAll = async () => {
    if (window.confirm(`Voulez vous supprimer tous les étudiants ?`)) {
      await dropTable();
    }
  };

  const handleGenerate = async () => {
    await generateStudents();
  };

  useEffect(() => {
    if (FeedBackMSG) {
      alert(FeedBackMSG);
    }
    return setFeedBackMSG();
  }, [FeedBackMSG]);

  return (
    <Card className="homeCards">
      <Card.Header as="h4" className="blue-heading">
        <div style={{ display: "flex" }}>
          <p className="me-auto"> Etudiants </p>
          <Button variant="danger" style={{ marginRight: "4px", maxHeight: "38px" }} onClick={deleteAll}>
            <JournalX size={20} /> Supprimer tout
          </Button>
          <Button variant="success" style={{ marginRight: "4px", maxHeight: "38px" }} onClick={handleGenerate}>
            <PersonPlusFill size={20} /> +100 Aléatoire
          </Button>
          <Link to="/students/create">
            <Button variant="info">
              <PersonPlusFill size={20} /> Nouvel étudiant
            </Button>
          </Link>
        </div>
      </Card.Header>

      <Card.Body>
        <Row>
          <Col lg="12">
            <TableComp data={data} deleteOne={deleteOne} />
          </Col>
        </Row>
      </Card.Body>
    </Card>
  );
}
