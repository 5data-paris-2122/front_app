import React from "react";
import { Form } from "react-bootstrap";

export default function FormSelect({ label, id, disabled, onChange, options }) {
  return (
    <Form.Select aria-label={label} id={id} disabled={disabled} onChange={onChange}>
      {options.map((option, i) => (
        <option value={option} key={i}>
          {option}
        </option>
      ))}
    </Form.Select>
  );
}
