import React from "react";
import { Row, Col } from "react-bootstrap";
import { FileEarmarkPlusFill, FileEarmarkMinusFill } from "react-bootstrap-icons";
import FormControl from "./formControl";
import FormCheck from "./formCheck";
import BlocAddModules from "./blocAddModules";

export default function BlocAddStudentCursus({ name, target, handleModifyState, count, params, contract, modules, disabled, student }) {
  function getCount(oldCount) {
    if (!student) return oldCount;
    return oldCount.length;
  }
  function getPlaceHolder(basicPlaceHolder, id, i) {
    if (student) {
      if (id.includes("supcursus_name")) return student.sup_cursus[i].class_name;
      if (id.includes("supcursus_campus")) return student.sup_cursus[i].campus;
      if (id.includes("supcursus_attendance")) return student.sup_cursus[i].attendance;
      if (id.includes("supcursus_attended_open_day")) return student.sup_cursus[i].attended_open_day;
      if (id.includes("supcursus_contract_title")) return student.sup_cursus[i].contract.job_title;
      if (id.includes("supcursus_contract")) return student.sup_cursus[i].contract.company_name;
    } else {
      return basicPlaceHolder;
    }
  }

  return (
    <div style={{ marginBottom: "6px" }}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <h6>{name} :</h6>
        {disabled ? null : (
          <div style={{ display: "flex", justifyContent: "space-between", margin: "4px 0px" }}>
            <button style={{ border: "0px", backgroundColor: "transparent" }} type="button" onClick={() => handleModifyState(target + ":+")}>
              <FileEarmarkPlusFill size={22} color="gray" />
            </button>
            <button style={{ border: "0px", backgroundColor: "transparent" }} type="button" onClick={() => handleModifyState(target + ":-")}>
              <FileEarmarkMinusFill size={22} color="gray" />
            </button>
          </div>
        )}
      </div>
      <div style={{ border: "1px solid #E7EBEB", borderRadius: "12px", padding: "8px", boxShadow: "0 4px 16px 0 rgb(25 35 37 / 8%)" }}>
        {[...Array(count).keys()].map((_, i) => (
          <Row key={i} style={{ marginBottom: "8px" }}>
            <h6 style={{ textAlign: "center" }}>Cursus : {i + 1}</h6>
            <h6>Informations générales :</h6>
            <div>
              <Row style={{ margin: "0 0 4px 12px", backgroundColor: "rgb(86 71 60 / 12%)", padding: "8px", borderRadius: "12px" }}>
                {params.map(({ label, placeholder, type, id }, j) =>
                  type === "check" ? (
                    <Col lg={6} key={j} style={{ margin: "auto" }}>
                      <FormCheck label={label} type={type} defaultChecked={getPlaceHolder(false, id, i)} id={id + i} disabled={disabled} />
                    </Col>
                  ) : (
                    <Col lg={6} key={j}>
                      <FormControl label={label} type={type} placeholder={getPlaceHolder(placeholder, id, i)} id={id + i} disabled={disabled} />
                    </Col>
                  )
                )}
              </Row>
            </div>
            <h6>Contrat alternance :</h6>
            <div>
              <div style={{ margin: "0 0 4px 12px", backgroundColor: "rgb(86 71 60 / 12%)", padding: "8px", borderRadius: "12px" }}>
                <Row>
                  {contract.map(({ label, type, placeholder, id }, j) => (
                    <Col lg={6} key={j}>
                      <FormControl label={label} type={type} placeholder={getPlaceHolder(placeholder, id, i)} id={id + i} disabled={disabled} />
                    </Col>
                  ))}
                </Row>
              </div>
            </div>
            {modules.map(({ name, target, count, params }, j) => (
              <BlocAddModules
                student={student}
                disabled={disabled}
                key={j}
                supcursNumber={i}
                name={name}
                target={target}
                handleModifyState={handleModifyState}
                params={params}
                count={getCount(count[i].modules)}
              />
            ))}
            {count > 1 && i < count - 1 ? <hr style={{ maxWidth: "250px", margin: "4px auto" }} /> : null}
          </Row>
        ))}
      </div>
    </div>
  );
}
