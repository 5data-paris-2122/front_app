import React from "react";
import { Row, Col } from "react-bootstrap";
import { FileEarmarkPlusFill, FileEarmarkMinusFill } from "react-bootstrap-icons";
import FormControl from "./formControl";

export default function BlocAddStudent({ name, target, handleModifyState, count, params, disabled, student }) {
  function getPlaceHolder(basicPlaceHolder, id, i) {
    if (student) {
      if (id.includes("edu")) {
        if (id === "edu:") return student.education[i].schoolname;
        if (id === "edu_degree:") return student.education[i].degrees;
      }
      if (id.includes("intern")) {
        if (id === "intern:") return student.internships[i].company_name;
        if (id === "intern_date:") return student.internships[i].year;
      }
    } else {
      return basicPlaceHolder;
    }
  }

  return (
    <div style={{ marginBottom: "6px" }}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <h6>{name} :</h6>
        {disabled ? null : (
          <div style={{ display: "flex", justifyContent: "space-between", margin: "4px 0px" }}>
            <button style={{ border: "0px", backgroundColor: "transparent" }} type="button" onClick={() => handleModifyState(target + ":+")}>
              <FileEarmarkPlusFill size={22} color="gray" />
            </button>
            <button style={{ border: "0px", backgroundColor: "transparent" }} type="button" onClick={() => handleModifyState(target + ":-")}>
              <FileEarmarkMinusFill size={22} color="gray" />
            </button>
          </div>
        )}
      </div>
      <div style={{ border: "1px solid #E7EBEB", borderRadius: "12px", padding: "8px", boxShadow: "0 4px 16px 0 rgb(25 35 37 / 8%)" }}>
        {[...Array(count).keys()].map((_, i) => (
          <Row key={i}>
            {params.map(({ label, type, placeholder, id }, j) => (
              <Col lg={6} key={j}>
                <FormControl label={label} type={type} placeholder={getPlaceHolder(placeholder, id, i)} id={id + i} disabled={disabled} />
              </Col>
            ))}
            {count > 1 && i < count - 1 ? <hr style={{ maxWidth: "200px", margin: "0px auto" }} /> : null}
          </Row>
        ))}
      </div>
    </div>
  );
}
