import React from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import { Layout } from "./../../components";

export default function CustomRouterLayout({ routes, userData }) {
  return (
    <Layout userData={userData}>
      <Switch>
        {Object.keys(routes).map((page, i) => (
          <Route key={i} exact={routes[page].is_exact} path={routes[page].path}>
            {routes[page].comp}
          </Route>
        ))}
        <Redirect to="/" />
      </Switch>
    </Layout>
  );
}
