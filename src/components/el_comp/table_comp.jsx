import React from "react";
import { Table, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { EyeFill, TrashFill } from "react-bootstrap-icons";

export default function Table_comp({ data, deleteOne }) {
  const handleDelete = (e, element) => {
    e.preventDefault();
    deleteOne(element);
  };
  return (
    <Table striped bordered hover responsive>
      <thead style={{ textAlign: "center" }}>
        <tr>
          <th>Supinfo ID</th>
          <th>Prénom</th>
          <th>Nom</th>
          <th>Email</th>
          <th>Adresse</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {data.map((element, i) => (
          <tr key={i}>
            <td style={{ maxWidth: "120px", textAlign: "center" }}>{element.supId}</td>
            <td style={{ maxWidth: "120px", textAlign: "center" }}>{element.name}</td>
            <td style={{ maxWidth: "120px", textAlign: "center" }}>{element.lastname}</td>
            <td style={{ maxWidth: "120px", textAlign: "center" }}>{element.email}</td>
            <td
              style={{ maxWidth: "120px", textAlign: "center" }}
            >{`${element.address.street}, ${element.address.city.toUpperCase()}`}</td>
            <td style={{ maxWidth: "060px", textAlign: "center" }}>
              <Link to={`students/${element._id}`}>
                <Button variant="success">
                  <EyeFill />
                </Button>
              </Link>
              <Button variant="danger" style={{ marginLeft: "2px" }} onClick={(e) => handleDelete(e, element)}>
                <TrashFill />
              </Button>
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
}
