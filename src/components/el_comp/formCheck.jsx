import React from "react";
import { Form } from "react-bootstrap";

export default function FormCheck({ label, id, disabled, onClick, defaultChecked }) {
  return (
    <Form.Group className="mb-3" controlId={id}>
      <Form.Label>{label}</Form.Label>
      <Form.Check defaultChecked={defaultChecked} id={id} disabled={disabled} onClick={onClick} />
    </Form.Group>
  );
}
