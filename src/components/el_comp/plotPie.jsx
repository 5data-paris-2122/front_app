import { useEffect } from "react";
import * as d3 from "d3";

export default function PlotPie({ data }) {
  function effect() {
    const margin = { top: 10, right: 30, bottom: 90, left: 40 };
    const width = 960 - margin.left - margin.right;
    const height = 450 - margin.top - margin.bottom;
    const radius = Math.min(width, height) / 2;

    const svg = d3
      .select("#plotPieTarget")
      .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .attr("id", "mainSVG")
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // X axis
    var x = d3
      .scaleBand()
      .range([0, width])
      .domain(
        data.map(function (d) {
          return d.Attr1;
        })
      )
      .padding(0.2);
    svg
      .append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x))
      .selectAll("text")
      .attr("transform", "translate(-10,0)rotate(-45)")
      .style("text-anchor", "end");

    // Add Y axis
    var y = d3.scaleLinear().domain([0, 20]).range([height, 0]);
    svg.append("g").call(d3.axisLeft(y));

    // Bars
    svg
      .selectAll("mybar")
      .data(data)
      .enter()
      .append("rect")
      .attr("x", function (d) {
        return x(d.Attr1);
      })
      .attr("width", x.bandwidth())
      .attr("fill", "#4040B8")
      // no bar at the beginning thus:
      .attr("height", function (d) {
        return height - y(0);
      }) // always equal to 0
      .attr("y", function (_) {
        return y(0);
      });

    // Animation
    svg
      .selectAll("rect")
      .transition()
      .duration(800)
      .attr("y", function (d) {
        return y(d.Attr2);
      })
      .attr("height", function (d) {
        return height - y(d.Attr2);
      })
      .delay(function (_, i) {
        return i * 100;
      });
  }

  function cleanup() {
    d3.select("#mainSVG").remove();
  }

  useEffect(() => {
    effect();
    return () => {
      cleanup();
    };
  }, [data]);

  return <div id="plotPieTarget" style={{ textAlign: "center", marginTop: "16px" }}></div>;
}
