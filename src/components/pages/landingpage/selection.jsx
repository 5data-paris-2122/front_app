import { useState, useEffect } from "react";
import { Row, Col } from "react-bootstrap";
import { FormSelect } from "./../../../middleware/components";

export default function Selection({ showGraph }) {
  const [FirstAttr, setFirstAttr] = useState();
  const [ScndAttr, setScndAttr] = useState();
  const [TypeGraph, setTypeGraph] = useState(0);

  const Attr1Options = ["Choisir un attribut", "Region", "Lycée", "Changement d'école", "Sortie de Supinfo", "Classes"];
  const Attr2Options = ["Choisir un attribut", "Notes", "Absences", "JPO", "Contrat Pro"];
  const types = ["Type de graphe", "Graphe à 1 attribut", "Graphe à 2 attributs"];

  const onChange = (event) => {
    if (event.target.id.includes("first")) {
      if (event.target.value === "Choisir un attribut") {
        setFirstAttr();
      } else {
        setFirstAttr(event.target.value);
      }
    }
    if (event.target.id.includes("second")) {
      if (event.target.value === "Choisir un attribut") {
        setScndAttr();
      } else {
        setScndAttr(event.target.value);
      }
    }
    if (event.target.id === "typeGraph") {
      if (event.target.value.includes(1)) {
        setTypeGraph(1);
        setScndAttr();
      }
      if (event.target.value.includes(2)) setTypeGraph(2);
      if (event.target.value === "Type de graphe") {
        setTypeGraph(0);
        setFirstAttr();
        setScndAttr();
      }
    }
  };

  useEffect(() => {
    switch (TypeGraph) {
      case 0:
        showGraph({ typeGraph: TypeGraph, attributes: [] });
        break;
      case 1:
        if (FirstAttr) {
          showGraph({ typeGraph: TypeGraph, attributes: [FirstAttr] });
        } else {
          showGraph({ typeGraph: 0, attributes: [] });
        }
        break;
      case 2:
        if (FirstAttr && ScndAttr) {
          showGraph({ typeGraph: TypeGraph, attributes: [FirstAttr, ScndAttr] });
        } else {
          showGraph({ typeGraph: 0, attributes: [] });
        }
        break;

      default:
        break;
    }
    return () => {};
  }, [TypeGraph, FirstAttr, ScndAttr]);

  return (
    <Row>
      <Col lg="12" style={{ marginBottom: "16px" }}>
        <h6>Veuillez choisir le type de graphe souhaité :</h6>
        <FormSelect label="Type de graphe" id="typeGraph" disabled={false} onChange={onChange} options={types} />
      </Col>
      {new Array(TypeGraph).fill().map((_, i) => (
        <Col lg="6" key={i}>
          <FormSelect
            label={i === 0 ? "Premier attribut" : "Second attribut"}
            id={i === 0 ? "firstAttr" : "secondAttr"}
            disabled={false}
            onChange={onChange}
            options={i === 0 ? Attr1Options : Attr2Options}
          />
        </Col>
      ))}
    </Row>
  );
}
