import React from "react";
import { Navbar, Container, Nav } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";

import { NavlinkOffcanvas } from "./../../middleware/components";
import { menu } from "./../../middleware/common";

export default function MainNavigation() {
  function get_links() {
    return (
      <Nav className="me-auto">
        {menu.map((element, i) => (
          <LinkContainer key={i} to={element.href}>
            {element.children.length === 0 ? <Nav.Link>{element.title}</Nav.Link> : <NavlinkOffcanvas element={element} />}
          </LinkContainer>
        ))}
      </Nav>
    );
  }
  return (
    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
      <Container>
        <LinkContainer to="/">
          <Navbar.Brand>SupData</Navbar.Brand>
        </LinkContainer>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">{get_links()}</Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
