import { useState } from "react";
import { Nav, Offcanvas, Accordion, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Navlink_offcanvas({ element }) {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <>
      <Nav.Link onClick={handleShow}>{element.title}</Nav.Link>

      <Offcanvas show={show} onHide={handleClose}>
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>{element.title}</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
          <Accordion>
            {element.children.map((child, i) => (
              <Accordion.Item key={i} eventKey={i}>
                <Accordion.Header>{child.title}</Accordion.Header>
                {child.children.map((child, j) => (
                  <Link key={i + "_" + j} to={child.href}>
                    <Accordion.Body style={{ padding: "0.30rem 1.25rem" }}>
                      <Button variant="transparent" style={{ width: "100%" }}>
                        {child.title}
                      </Button>
                    </Accordion.Body>
                  </Link>
                ))}
              </Accordion.Item>
            ))}
          </Accordion>
        </Offcanvas.Body>
      </Offcanvas>
    </>
  );
}
