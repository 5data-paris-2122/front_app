import { useState } from "react";
import { Card, Row, Col } from "react-bootstrap";
import Selection from "./selection";
import Graphs from "./graphs";
import OverAllGraph from "./overAllGraph";
// import {Loading as LoadingComp} from "./../../../middleware/components"

export default function LandingPage({ students }) {
  const [TypeGraph, setTypeGraph] = useState(false);
  const [Attributes, setAttributes] = useState([]);

  const showGraph = ({ typeGraph, attributes }) => {
    setTypeGraph(typeGraph);
    setAttributes(attributes);
  };

  return (
    <Card className="homeCards">
      <Card.Header as="h4" className="blue-heading">
        Vue d'ensemble
      </Card.Header>

      <Card.Body>
        <OverAllGraph students={students} />
        <Row>
          <Col lg="12">
            <Selection showGraph={showGraph} />
          </Col>
        </Row>
        {!TypeGraph > 0 ? (
          <div style={{ minHeight: "100px" }}></div>
        ) : (
          <Row>
            <Col lg="12">
              <Graphs typeGraph={TypeGraph} attributes={Attributes} students={students} />
            </Col>
          </Row>
        )}
      </Card.Body>
    </Card>
  );
}
