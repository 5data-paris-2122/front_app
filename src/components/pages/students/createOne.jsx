import { useEffect, useState } from "react";
import { Card, Row, Col, Button, Form } from "react-bootstrap";
import { ArrowLeft } from "react-bootstrap-icons";
import { Link } from "react-router-dom";

import { BlocAddStudent, BlocAddStudentCursus, FormControl } from "./../../../middleware/components";

const initState = {
  education: 1,
  internships: 1,
  sup_cursus: [
    {
      modules: 1,
    },
  ],
};

export default function CreateOne({ createStudent, FeedBackMSG, setFeedBackMSG }) {
  const [state, setState] = useState(initState);
  const handleModifyState = (modification, supcursNumber) => {
    let target = modification.split(":")[0];
    let action = modification.split(":")[1];
    let temp_data = { ...state };
    switch (target) {
      case "education":
        if (action === "+") {
          temp_data.education += 1;
        } else {
          if (temp_data.education > 1) temp_data.education -= 1;
        }
        break;
      case "internship":
        if (action === "+") {
          temp_data.internships += 1;
        } else {
          if (temp_data.internships > 1) temp_data.internships -= 1;
        }
        break;
      case "sup_cursus":
        if (action === "+") {
          temp_data.sup_cursus.push({
            modules: 1,
          });
        } else {
          if (temp_data.sup_cursus.length > 1) temp_data.sup_cursus.pop();
        }
        break;
      case "modules":
        if (action === "+") {
          temp_data.sup_cursus[supcursNumber].modules += 1;
        } else {
          if (temp_data.sup_cursus[supcursNumber].modules > 1) temp_data.sup_cursus[supcursNumber].modules -= 1;
        }
        break;
      default:
        break;
    }
    setState(temp_data);
  };

  const parseStudent = (data) => {
    let student = {
      supId: data.supId,
      name: data.name,
      lastname: data.lastname,
      birthdate: data.birthdate,
      address: {
        street: data.street,
        city: data.city,
        country: data.country,
      },
      education: [],
      internships: [],
      sup_cursus: [],
      post_cursus: {
        future_job: {
          job_title: data.future_job_job_title,
          company_name: data.future_job_company_name,
        },
        quit_reason: data.quit_reason,
        new_school: data.new_school,
      },
    };
    for (let i = 0; i < Object.keys(data).length; i++) {
      const key = Object.keys(data)[i];
      if (key.includes("edu")) {
        let finalKey = key.split(":")[0];
        let count = key.split(":")[1];
        if (!student.education[count]) student.education[count] = {};
        if (finalKey === "edu") {
          student.education[count].schoolname = data[key];
        } else {
          student.education[count].degrees = data[key];
        }
      }
      if (key.includes("intern")) {
        let finalKey = key.split(":")[0];
        let count = key.split(":")[1];
        if (!student.internships[count]) student.internships[count] = {};
        if (finalKey === "intern") {
          student.internships[count].company_name = data[key];
        } else {
          student.internships[count].year = new Date(data[key]).getFullYear();
        }
      }
      if (key.includes("supcursus_")) {
        let fullKey = key.split(":")[0];
        let fullCount = key.split(":")[1];
        if (!fullKey.includes("modules")) {
          if (!student.sup_cursus[fullCount]) student.sup_cursus[fullCount] = {};
          if (!student.sup_cursus[fullCount].contract) student.sup_cursus[fullCount].contract = {};
          switch (fullKey) {
            case "supcursus_name":
              student.sup_cursus[fullCount].class_name = data[key];
              break;
            case "supcursus_campus":
              student.sup_cursus[fullCount].campus = data[key];
              break;
            case "supcursus_attendance":
              student.sup_cursus[fullCount].attendance = data[key];
              break;
            case "supcursus_attended_open_day":
              student.sup_cursus[fullCount].attended_open_day = data[key];
              break;
            case "supcursus_contract":
              student.sup_cursus[fullCount].contract.company_name = data[key];
              break;
            case "supcursus_contract_title":
              student.sup_cursus[fullCount].contract.job_title = data[key];
              break;
            default:
              break;
          }
        } else {
          let supCount = fullCount.split("_")[0];
          let count = fullCount.split("_")[1];
          if (!student.sup_cursus[supCount]) {
            student.sup_cursus[supCount] = {};
            student.sup_cursus[supCount].modules = [];
          }
          if (!student.sup_cursus[supCount].modules) {
            student.sup_cursus[supCount].modules = [];
          }
          if (!student.sup_cursus[supCount].modules[count]) {
            student.sup_cursus[supCount].modules[count] = {};
          }
          if (fullKey === "supcursus_modules") {
            student.sup_cursus[supCount].modules[count].name = data[key];
          } else {
            student.sup_cursus[supCount].modules[count].grades = data[key];
          }
        }
      }
    }
    return student;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    let result = {};
    for (let i = 0; i < e.target.length; i++) {
      const element = e.target[i];
      if (element.type !== "button" && element.type !== "submit") {
        result[element.id] = element.value;
      }
    }
    let student = parseStudent(result);
    await createStudent(student);
  };

  useEffect(() => {
    if (FeedBackMSG) {
      alert(FeedBackMSG.message);
      document.getElementById("formReset").reset();
      setState(initState);
    }
    return setFeedBackMSG();
  }, [FeedBackMSG]);

  return (
    <Card className="homeCards">
      <Card.Header as="h4" className="blue-heading">
        <div style={{ display: "flex" }}>
          <p className="me-auto"> Nouvel étudiant </p>
          <Link to="/students">
            <Button variant="info">
              <ArrowLeft size={20} /> Retour
            </Button>
          </Link>
        </div>
      </Card.Header>

      <Card.Body>
        <Row>
          <Col lg="12">
            <Form onSubmit={handleSubmit} id="formReset">
              <Row className="justify-content-center">
                <Col lg="6">
                  <Row>
                    <Col lg="6">
                      <FormControl label="Supinfo ID" type="text" placeholder="284126" id="supId" />
                      <FormControl label="Prénom" type="text" placeholder="Jean" id="name" />
                    </Col>
                    <Col lg="6">
                      <FormControl label="Nom" type="text" placeholder="Larry" id="lastname" />
                      <FormControl label="Date de naissance" type="date" id="birthdate" />
                    </Col>
                  </Row>
                  <h6>Adresse de résidence :</h6>
                  <Row>
                    <Col lg="12">
                      <FormControl label="Rue" type="text" placeholder="5 rue de la paix" id="street" />
                    </Col>
                    <Col lg="6">
                      <FormControl label="Ville" type="text" placeholder="Paris" id="city" />
                    </Col>
                    <Col lg="6">
                      <FormControl label="Pays" type="text" placeholder="France" id="country" />
                    </Col>
                  </Row>
                  <div
                    style={{
                      border: "1px solid #E7EBEB",
                      borderRadius: "12px",
                      padding: "8px",
                      boxShadow: "0 4px 16px 0 rgb(25 35 37 / 8%)",
                      backgroundColor: "rgb(86 71 60 / 12%)",
                    }}
                  >
                    <h6>Post cursus :</h6>
                    <div style={{ margin: "0 0 4px 12px", backgroundColor: "rgb(60 143 80 / 12%)", padding: "8px", borderRadius: "12px" }}>
                      <Row>
                        <h6 style={{ fontSize: "medium" }}>Poste occupé apres Supinfo cursus :</h6>
                        <Col lg="6">
                          <FormControl label="Titre du poste" type="text" placeholder="Développeur junior" id="future_job_job_title" />
                        </Col>
                        <Col lg="6">
                          <FormControl label="Nom de l'entreprise" type="text" placeholder="Google" id="future_job_company_name" />
                        </Col>
                      </Row>
                    </div>
                    <div style={{ margin: "0 0 4px 12px", backgroundColor: "rgb(191 47 35 / 12%)", padding: "8px", borderRadius: "12px" }}>
                      <FormControl label="Nouvelle école si changement d'école" type="text" placeholder="Epitech" id="new_school" />
                    </div>
                    <div style={{ margin: "0 0 4px 12px", backgroundColor: "rgb(191 47 35 / 12%)", padding: "8px", borderRadius: "12px" }}>
                      <FormControl label="Raison de sortie de Supinfo sinon" type="text" placeholder="Diplomé.e / Maladie" id="quit_reason" />
                    </div>
                  </div>
                </Col>
                <Col lg="6">
                  <BlocAddStudent
                    disabled={false}
                    name="Education"
                    target="education"
                    handleModifyState={handleModifyState}
                    count={state.education}
                    params={[
                      {
                        label: "Nom",
                        type: "text",
                        placeholder: "Epitech",
                        id: "edu:",
                        idbis: "",
                      },
                      {
                        label: "Diplome",
                        type: "text",
                        placeholder: "Bac",
                        id: "edu_degree:",
                      },
                    ]}
                  />
                  <BlocAddStudent
                    disabled={false}
                    name="Internships"
                    target="internship"
                    handleModifyState={handleModifyState}
                    count={state.internships}
                    params={[
                      {
                        label: "Nom de la société",
                        type: "text",
                        placeholder: "IONIS Corp",
                        id: "intern:",
                        idbis: "",
                      },
                      {
                        label: "Année",
                        type: "number",
                        placeholder: "2021",
                        id: "intern_date:",
                      },
                    ]}
                  />
                  <BlocAddStudentCursus
                    disabled={false}
                    name="Supinfo cursus"
                    target="sup_cursus"
                    handleModifyState={handleModifyState}
                    count={state.sup_cursus.length}
                    params={[
                      {
                        label: "Classe",
                        type: "text",
                        placeholder: "A.Sc1",
                        id: "supcursus_name:",
                      },
                      {
                        label: "Campus",
                        type: "text",
                        placeholder: "Paris",
                        id: "supcursus_campus:",
                      },
                      {
                        label: "Absences",
                        type: "number",
                        placeholder: "2",
                        id: "supcursus_attendance:",
                      },
                      {
                        label: "JPO",
                        type: "check",
                        id: "supcursus_attended_open_day:",
                      },
                    ]}
                    contract={[
                      {
                        label: "Nom de la société",
                        type: "text",
                        placeholder: "Microsoft",
                        id: "supcursus_contract:",
                      },
                      {
                        label: "Titre du profil",
                        type: "text",
                        placeholder: "Cloud functions dev",
                        id: "supcursus_contract_title:",
                      },
                    ]}
                    modules={[
                      {
                        name: "Modules",
                        target: "modules",
                        count: state.sup_cursus,
                        params: [
                          {
                            label: "Nom",
                            type: "text",
                            placeholder: "5Data",
                            id: "supcursus_modules:",
                          },
                          {
                            label: "Note",
                            type: "number",
                            placeholder: "14",
                            id: "supcursus_modules_grade:",
                          },
                        ],
                      },
                    ]}
                  />
                  <Button variant="primary" type="submit" style={{ float: "right" }}>
                    Créer
                  </Button>
                </Col>
              </Row>
            </Form>
          </Col>
        </Row>
      </Card.Body>
    </Card>
  );
}
