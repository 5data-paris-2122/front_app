import React from "react";
import { Form } from "react-bootstrap";

export default function FormControl({ label, type, placeholder, id, disabled, defaultValue }) {
  return (
    <Form.Group className="mb-3" controlId={id}>
      <Form.Label>{label}</Form.Label>
      <Form.Control type={type} placeholder={placeholder} disabled={disabled} defaultValue={defaultValue} />
    </Form.Group>
  );
}
